import React, { useCallback, useState } from "react";
import "./App.css";
import LinkIcon from "@mui/icons-material/Link";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import { IconButton, InputBase } from "@material-ui/core";
import {useDropzone} from 'react-dropzone'

import WebTorrent from "./components/WebTorrent/WebTorrent";

interface IMyDropzoneProps {
  onChange: (arg0: string|any) => void;
}

function MyDropzone(props: IMyDropzoneProps) {
  const onDrop = useCallback(acceptedFiles => {
    //console.log(acceptedFiles[0]);
    props.onChange(acceptedFiles[0]);
  }, [])
  const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})
  
  return (
    <div className="torrent-dropzone" {...getRootProps()}>
      <input {...getInputProps()} />
      {
        isDragActive ?
        <p>Drop the files here ...</p> :
          <p>Drag n drop some files here, or click to select files</p>
      }
    </div>
  )
}

interface ISearchBar {
  input: string|any;
  onChange: (arg0: string|any) => void;
  setShowPlayer: (arg0: boolean) => void;
}

function SearchBar(props: ISearchBar) {
  return (
    <div>
      <div className="search-bar">
        <InputBase
          placeholder="insert magnet torrent"
          className="MagnetTorrentInput"
          value={props.input}
          style={{ width: "100%" }}
          onChange={(e) => props.onChange(e.target.value)}
          inputProps={{ "aria-label": "insert magnet torrent" }}
        />
        <IconButton>
          <LinkIcon />
        </IconButton>
      </div>
      <MyDropzone onChange={props.onChange} />
      {props.input !== "" && (
        <IconButton onClick={() => props.setShowPlayer(true)}>
          Play
          <PlayArrowIcon />
        </IconButton>
      )}
    </div>
  );
}

function App() {
  const [input, setInput] = useState<string|any>("");
  const [showPlayer, setShowPlayer] = useState(false);

  return (
    <div className="App">
      <header className="App-header"></header>
      <div className="App-body">
        {showPlayer ? (
          <WebTorrent showPlayer={showPlayer} torrentMagnet={input} />
        ) : (
          <SearchBar
            input={input}
            onChange={setInput}
            setShowPlayer={setShowPlayer}
          />
        )}
      </div>
    </div>
  );
}

export default App;
