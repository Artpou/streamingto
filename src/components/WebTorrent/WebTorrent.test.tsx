import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import WebTorrent from './WebTorrent';

describe('<WebTorrent />', () => {
  test('it should mount', () => {
    render(<WebTorrent />);
    
    const webTorrent = screen.getByTestId('WebTorrent');

    expect(webTorrent).toBeInTheDocument();
  });
});