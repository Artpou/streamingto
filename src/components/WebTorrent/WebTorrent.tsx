import React, { useEffect } from "react";
import "./WebTorrent.css";
import WebTorrent from "webtorrent";
import TorrentFile from "../TorrentFile/TorrentFile";
import { Container } from "@mui/material";
import TorrentFileVideo from "../TorrentFile/TorrentFileVideo";

interface IWebTorrentProps {
  showPlayer: boolean;
  torrentMagnet: string | any;
}

enum TorrentFileType {
  video,
  other,
}

interface ITorrentFile {
  content: any;
  type: TorrentFileType;
}

function WebTorrentC(props: IWebTorrentProps): JSX.Element {
  const [done, setDone] = React.useState(0);
  const [files, setFiles] = React.useState<Array<any>>([]);
  const [subtitlesUrl, setSubtitlesUrl] = React.useState<Array<string>>([]);

  useEffect(() => {
    const client = new WebTorrent();
    const torrentId = props.torrentMagnet;

    client.add(torrentId, function (torrent: any) {
      setInterval(() => {
        const progress: number = +(+torrent.progress * 100).toFixed(2);
        setDone(progress);
      }, 1000);

      torrent.files.forEach(function (file: any) {
        let type: TorrentFileType = TorrentFileType.other;

        if (file.name.endsWith(".srt")) {
          file.getBlobURL(function (err: any, url: string) {
            setSubtitlesUrl((oldArray) => [...oldArray, url]);
          });
        } else if (file.name.endsWith(".mp4")) {
          type = TorrentFileType.video;
        }

        switch (type) {
          case TorrentFileType.video:
            setFiles((files) => [{ content: file, type: TorrentFileType.video }, ...files]);      
            break;
        
          default:
            setFiles((files) => [...files, { content: file, type: TorrentFileType.other }]);
            break;
        }
      });
    });
  }, [props.showPlayer == true]);

  useEffect(() => {
    console.log(files);
  }, [files]);

  return (
    <Container className="WebTorrent">
      <p>Done : {done} %</p>
      {files.map((file: any, index: number) => {
        <div key={index}>test</div>
      })}
      {files.map((file: ITorrentFile, index: number) => {
        return (
          file.type == TorrentFileType.video ? (
            <TorrentFileVideo file={file.content} subtitles={subtitlesUrl} />
          ) : (
            <TorrentFile file={file.content} />
          )
        )
      })}
    </Container>
  );
}

export default WebTorrentC;
