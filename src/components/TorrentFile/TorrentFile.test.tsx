import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import TorrentFile from './TorrentFile';

describe('<TorrentFile />', () => {
  test('it should mount', () => {
    render(<TorrentFile />);
    
    const torrentFile = screen.getByTestId('TorrentFile');

    expect(torrentFile).toBeInTheDocument();
  });
});