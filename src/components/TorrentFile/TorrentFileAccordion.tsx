import React from 'react';
import {
    Accordion,
    AccordionSummary,
    Typography,
    AccordionDetails,
  } from "@mui/material";
  import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

function TorrentFileAccordion(props:any) {
    return (
        <Accordion style={{marginBottom: 10}} defaultExpanded={props.expanded}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
        <Typography>{props.name}</Typography>
        </AccordionSummary>
        <AccordionDetails>
            {props.children}
        </AccordionDetails>
      </Accordion>
    );
}

export default TorrentFileAccordion;