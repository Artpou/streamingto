import React, { useEffect } from "react";
import { toClassName } from "./utils";
import "./TorrentFile.css";
import TorrentFileAccordion from "./TorrentFileAccordion";

interface ITorrentFileVideoProps {
  file: any;
  subtitles?: Array<string>;
}

function TorrentFileVideo(props: ITorrentFileVideoProps) {
  useEffect(() => {
    props.file.renderTo("#" + toClassName(props.file.name));
  }, [props.file]);

  useEffect(() => {
      console.log(props.subtitles);
  }, [props.subtitles]);

  return (
    <TorrentFileAccordion name={props.file.name} expanded={true}>
      {props.file !== undefined && <video id={toClassName(props.file.name)} controls>    
        {props.subtitles &&
          props.subtitles.map(function (url: string, key: number) {
            return <track kind="subtitles" key={key} src={url} />;
          })}
      </video>}
    </TorrentFileAccordion>
  );
}

export default TorrentFileVideo;
