export function toClassName(name: string): string {
    return name.replaceAll(".", "_");
}