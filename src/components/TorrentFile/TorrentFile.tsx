import React, { useEffect } from "react";
import "./TorrentFile.css";
import TorrentFileAccordion from "./TorrentFileAccordion";
import { toClassName } from "./utils";

interface ITorrentFileProps {
  file: any;
}

function TorrentFile(props: ITorrentFileProps) {
  useEffect(() => {
    console.log(props.file);
    props.file.appendTo("#" + toClassName(props.file.name));
  }, [props.file]);

  return (
    <TorrentFileAccordion name={props.file.name} expanded={false}>
      {props.file !== undefined && <div id={toClassName(props.file.name)} />}
    </TorrentFileAccordion>
  );
}

export default TorrentFile;
